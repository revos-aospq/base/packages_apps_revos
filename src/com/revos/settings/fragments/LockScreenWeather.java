/*
 *  Copyright (C) 2015 The OmniROM Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.revos.settings.fragments;

import com.android.internal.logging.nano.MetricsProto;

import android.app.Activity;
import android.content.Context;
import android.content.ContentResolver;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.res.Resources;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import androidx.preference.SwitchPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceScreen;

import android.provider.Settings;
import com.android.settings.R;
import com.android.settings.SettingsPreferenceFragment;
import com.revos.settings.preference.SystemSettingListPreference;
import com.revos.settings.preference.SystemSettingSwitchPreference;
import com.revos.settings.preference.SystemSettingSeekBarPreference;
import com.revos.settings.preference.Utils;


public class LockScreenWeather extends SettingsPreferenceFragment implements
    Preference.OnPreferenceChangeListener {

    private static final String KEY_LOCKSCREEN_WEATHER_ENABLED = "lockscreen_weather_enabled";
    private static final String KEY_LOCKSCREEN_WEATHER_STYLE = "lockscreen_weather_style";
    private static final String KEY_LOCKSCREEN_WEATHER_CITY = "lockscreen_weather_show_city";
    private static final String KEY_LOCKSCREEN_WEATHER_TEMP = "lockscreen_weather_show_temp";

    private SystemSettingSwitchPreference mLockscreenWeatherCity;
    private SystemSettingSwitchPreference mLockscreenWeatherTemp;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        addPreferencesFromResource(R.xml.revos_settings_lockscreen_weather);

        ContentResolver resolver = getActivity().getContentResolver();
        final PreferenceScreen prefScreen = getPreferenceScreen();
        Resources resources = getResources();

        mLockscreenWeatherCity = (SystemSettingSwitchPreference) findPreference(KEY_LOCKSCREEN_WEATHER_CITY);
        mLockscreenWeatherTemp = (SystemSettingSwitchPreference) findPreference(KEY_LOCKSCREEN_WEATHER_TEMP);

        mLockscreenWeatherTemp.setEnabled(isOmniWeatherEnabled());
        mLockscreenWeatherCity.setEnabled(isOmniWeatherEnabled());
    }


    private boolean isOmniWeatherEnabled() {
        boolean isWeatherEnabled = Settings.System.getInt(getContentResolver(),
                Settings.System.OMNI_LOCKSCREEN_WEATHER_ENABLED, 1) == 1;
        boolean isPixelStyleEnabled = Settings.System.getInt(getContentResolver(),
                Settings.System.LOCKSCREEN_WEATHER_STYLE, 1) == 1;
        return isWeatherEnabled && !isPixelStyleEnabled;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        ContentResolver resolver = getActivity().getContentResolver();
        switch (preference.getKey()) {
            case KEY_LOCKSCREEN_WEATHER_ENABLED:
            case KEY_LOCKSCREEN_WEATHER_STYLE:
                mLockscreenWeatherTemp.setEnabled(isOmniWeatherEnabled());
                mLockscreenWeatherCity.setEnabled(isOmniWeatherEnabled());
                return true;
            default:
                return false;
        }
    }

    @Override
    public int getMetricsCategory() {
        return MetricsProto.MetricsEvent.REVOS_SETTINGS;
    }

}
